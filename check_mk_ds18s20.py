#!/usr/bin/env python

from ds18s20 import ds18s20

sensortable = {
'10-0008030b08dd': { 'label': 'temp_bier',    'lowcrit': 3,  'lowwarn': 4,  'highwarn': 9,  'highcrit': 10 },
'10-00080153803a': { 'label': 'temp_kantoor', 'lowcrit': 15, 'lowwarn': 18, 'highwarn': 24, 'highcrit': 28 },
'10-0008030ae5ca': { 'label': 'temp_werkhok', 'lowcrit': 15, 'lowwarn': 18, 'highwarn': 24, 'highcrit': 28 },
}

STATUS_OK=0
STATUS_WARNING=1
STATUS_CRITICAL=2
STATUS_UNKNOWN=3


if __name__ == '__main__':
   for sensor in ds18s20():
      status = STATUS_OK
      temp = sensor.read_temp()
      tempmsg = "%.1f%sC" % (temp, u"\u00B0".encode('utf8'))
      msg = tempmsg
      # check if the sensor is known
      if sensor.get_id() in sensortable:
         sensor_info = sensortable[sensor.get_id()]

         range = '%.1f:%.1f;%.1f:%.1f;%.1f;%.1f' % (sensor_info['lowwarn'], sensor_info['highwarn'], sensor_info['lowcrit'], sensor_info['highcrit'], 0, 40)

         # low thresholds
         if temp < sensor_info['lowwarn']:
            status = STATUS_WARNING
            msg = "low temperature, %s" % tempmsg
     
         if temp < sensor_info['lowcrit']:
            status = STATUS_CRITICAL
            msg = "low temperature, %s" % tempmsg

         # high thresholds
         if temp > sensor_info['highwarn']:
            status = STATUS_WARNING
            msg = "high temperature, %s" % tempmsg
     
         if temp > sensor_info['highcrit']:
            status = STATUS_CRITICAL
            msg = 'high temperature, %s' % tempmsg
   
         # output according to https://mathias-kettner.de/checkmk_localchecks.html#How%20to%20write%20local%20checks
         print "%s %s temperature_in_celsius=%f;%s %s" % (status, sensor_info['label'], temp, range, msg)
      else:
         print "%s %s temperature_in_celsius=%f;%s" % (status, sensor.get_id(), temp, msg)

