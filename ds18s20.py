#!/usr/bin/env python
# ds18s20.py

import os
import glob
import time

class ds18s20:
  
   def __init__(self):
      # Find file names for the sensor(s)
      base_dir = '/sys/bus/w1/devices/'
      self._device_files = glob.glob(base_dir + '10*' + '/w1_slave')
      self.current = len(self._device_files)

   def __iter__(self):
      return self

   def next(self):
      if self.current <= 0:
         raise StopIteration
      else:
         self.current -= 1
         return self

   def _read_temp_raw(self):
      # Issue one read to one sensor
      # you should not call this directly
      try:
         f = open(self._device_files[self.current], 'r')
      except IOError:
         print 'Cannot open ', self._device_files[self.current]
      else:
         lines = f.readlines()
         f.close()
         return lines

   def get_id(self):
      return self._device_files[self.current].split("/")[-2]

   def read_temp(self):
      # call this to get the temperature in degrees C
      # detected by a sensor
      lines = self._read_temp_raw()

      # first line should end with 'YES'
      if lines[0].strip()[-3:] != 'YES':
         raise StandardError

      # second line should contain 't='
      equals_pos = lines[1].find('t=')
      if equals_pos == -1:
         raise StandardError

      temp = lines[1][equals_pos + 2:]
      return float(temp)/1000



if __name__ == '__main__':
   for sensor in ds18s20():
      print "Sensor %s reading %f" % (sensor.get_id(), sensor.read_temp())
