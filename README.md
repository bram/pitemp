# pitemp
Python script to read temperatures from a 1wire network of `DS18S20` sensors and feed them into the Icinga monitoring system.

> Much of this code is lifted from [Adafruit web site](https://learn.adafruit.com/adafruits-raspberry-pi-lesson-11-ds18b20-temperature-sensing/software).


## Raspberry pi setup
It uses OS supplied drivers and 1wire support must be enabled.
To do this add the line:
```
dtoverlay=w1-gpio
```
to the end of `/boot/config.txt`.

## Wiring the sensors
The `DS18B20` has three pins, looking at the flat side with the pins pointing
down pin 1 is on the left.

![DS18S20 pinout](img/ds18s20.png)


### Connections

| sensor pin | description | connect to raspberry pi                                                   |
|------------|-------------|---------------------------------------------------------------------------|
| 1          | GND         | Ground (pin 6)                                                            |
| 2          | DQ          | GPIO 4 (pin) *and* GPIO 3.3V (pin 1) via a 4k8 (4800 ohm) pullup resistor |
| 3          | Vdd         | 3.3V (pin 1)                                                              |

> When connecting more sensors, use the same pins on the raspberry pi. Only one pullup resistor is required

## Usage
Run `ds18s20.py` to obtain a single reading from all connected sensors:
```
pi@raspberrypi:~/src/ds18b20 $ ./ds18s20.py 
Sensor 10-00080153803a reading 22.500000
Sensor 10-0008030ae5ca reading 22.562000
Sensor 10-0008030b08dd reading 5.250000
```

## Check_mk
The output of `check_mk_ds18s20b.py` looks like:
```
pi@raspberrypi:~/src/ds18b20 $ ./check_mk_ds18s20b.p
0 10-00080153803a temperature_in_celsius=22.500000;22.5°C
0 10-0008030ae5ca temperature_in_celsius=22.562000;22.6°C
0 10-0008030b08dd temperature_in_celsius=5.312000;5.3°C
```

The labels of the sensors and alarm thresholds can be adjusted by editing the `sensortable` array, which is self explanatory:
```
sensortable = {
'10-0008030b08dd': { 'label': 'temp_fridge',  'lowcrit': 3,  'lowwarn': 4,  'highwarn': 9,  'highcrit': 10 },
'10-00080153803a': { 'label': 'temp_inside',  'lowcrit': 15, 'lowwarn': 18, 'highwarn': 24, 'highcrit': 28 },
'10-0008030ae5ca': { 'label': 'temp_outside', 'lowcrit': -5, 'lowwarn': 18, 'highwarn': 24, 'highcrit': 28 },
}
```

Now the output looks like:
```
0 temp_inside temperature_in_celsius=22.500000;18.0:24.0;15.0:28.0;0.0;40.0 22.5°C
0 temp_outside temperature_in_celsius=22.562000;18.0:24.0;-5.0:28.0;0.0;40.0 22.6°C
0 temp_fridge temperature_in_celsius=5.562000;4.0:9.0;3.0:10.0;0.0;40.0 5.6°C
```

Make sure the check_mk-script is symlinked to `/usr/lib/check_mk_agent/local`, e.g.:
```
pi@raspberrypi:~ $ sudo ln -s ~/src/ds18b20/check_mk_ds18s20.py /usr/lib/check_mk_agent/local/
```

Now check_mk_agent will output temperatures in the `<<<local>>>` section.